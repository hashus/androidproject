<?php

require_once 'vendor/autoload.php';
require_once 'db/query.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if (false) {
    DB::$dbName = 'id8215000_challengerdb';
    DB::$user = 'id8215000_challengerdb';
    DB::$password = "challengerdbA&H";
    DB::$host = '127.0.0.1';
    DB::$port = 3306;
} else {
    DB::$dbName = 'id8215000_challengerdb';
    DB::$user = 'id8215000_challengerdb';
    DB::$password = "challengerdbA&H";
    DB::$host = 'localhost';
}


DB::$encoding = 'utf8';

DB::$error_handler = 'error_handler';
DB::$nonsql_error_handler = 'error_handler';

// returns FALSE if authentication is missing or invalid
// returns users table record of authenticated users if user/pass was correct
function getAuthUser() {
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        return FALSE;
    }
    $id = $_SERVER['PHP_AUTH_USER'];
    $authUser = DB::queryFirstRow("SELECT * FROM users WHERE id=%s", $id);
    if (!$authUser) {
        return FALSE;
    }
    return $authUser;
}




$app = new \Slim\Slim();
// this script will always return JSON  to any client

$app->response->header('content-type', 'application/json');


//TESTED -it gets all friends list- NEED AUTHENTICATION
// Get the friend list of the user

$app->get('/friends', function () {
    $friendsList = DB::query("SELECT name FROM friends,users WHERE users.id=friends.userId");
    echo json_encode($friendsList, JSON_PRETTY_PRINT);
});


// Get the friend list of the authenticated user
//$app->get('/friends', function() use ($app, $log) {
//    $authUser = getAuthUser();
//    if ($authUser === FALSE) {
//        $app->response()->status(401);
//        echo json_encode("401 - unauthorized (authentication missing or invalid)");
//        return;
//    }
//    $friendList = DB::query("SELECT name FROM friends,users WHERE users.id=friends.userId");
//    echo json_encode($friendList, JSON_PRETTY_PRINT);
//});
//TESTED works
// Get the user list of the top ten user no need for authentication

$app->get('/topTenUser', function () {
    $userList = DB::query("SELECT name, points FROM stats,users WHERE stats.userId = users.id ORDER BY points DESC");
    $topTenUser = array_slice($userList, 0, 10);
    echo json_encode($topTenUser, JSON_PRETTY_PRINT);
});



//TESTED works  NEED authentication
// Get the game history of the user
/*
$app->get('/gameHistory/:id', function ($id) use ($app) {

    //FIXME:We should show the user2.name  
    //and get the games if the state is finished - if the state is true- 

    $gameHistory = DB::query("SELECT u1.name as name1, u2.name as name2 "
                    . "FROM games "
                    . "JOIN users as u1 ON u1.id = games.user1Id "
                    . "JOIN users as u2 ON u2.id = games.user2Id "
                    . "WHERE games.user1Id = %i", $id);
    echo json_encode($gameHistory, JSON_PRETTY_PRINT);
});
*/

//TESTED
// Get all the game history of the all users
//$app->get('/gameHistory', function () {
//
//    //FIXME:We should show the user2.name  
//    //and get the games if the state is finished - if the state is true- 
//
//    $gameHistory = DB::query("SELECT u1.name as name1, u2.name as name2 "
//                    . "FROM games "
//                    . "JOIN users as u1 ON u1.id = games.user1Id "
//                    . "JOIN users as u2 ON u2.id = games.user2Id ");
//    echo json_encode($gameHistory, JSON_PRETTY_PRINT);
//});


//TESTED WORKS
// Get the Authenticated user's finished games  NAME1-VALUE1-TYPE-NAME2-VALUE2 

$app->get('/gameHistory/:userId', function ($userId) use($app){
    $gameList = DB::query("SELECT u1.name as name1, user1Value, type, u2.name as name2, user2Value "
                    . "FROM games "
                    . "LEFT OUTER JOIN users as u1 ON u1.id = games.user1Id "
                    . "LEFT OUTER JOIN users as u2 ON u2.id = games.user2Id "
                  ."WHERE (user1Id=%s OR user2Id=%s) AND games.state='done' ",$userId, $userId);
    echo json_encode($gameList, JSON_PRETTY_PRINT);
});


//TESTED WORKS
// Get the Authenticated user's outgoing pending games  NAME1-VALUE1-TYPE-NAME2 

$app->get('/outgoingChallenges/:userId', function($userId) use($app) {
    $gameList = DB::query("SELECT  u1.name as name1, user1Value, type, u2.name as name2 "
            . "FROM games "
            . "LEFT OUTER JOIN users as u1 ON u1.id = games.user1Id "
            . "LEFT OUTER JOIN users as u2 ON u2.id = games.user2Id "
            . "WHERE user1Id=%s && state=%s", $userId,'pending');
   

    echo json_encode($gameList, JSON_PRETTY_PRINT);
});

//TESTED WORKS
// Get the Authenticated user's incoming pending games  NAME1-NAME2 

$app->get('/incomingChallenges/:userId', function($userId) use($app) {
    $gameList = DB::query("SELECT u1.name as name1, u2.name as name2 "
            . "FROM games "
            . "LEFT OUTER JOIN users as u1 ON u1.id = games.user1Id "
            . "LEFT OUTER JOIN users as u2 ON u2.id = games.user2Id "
            . "WHERE user2Id=%s && state=%s", $userId,'pending');
   

    echo json_encode($gameList, JSON_PRETTY_PRINT);
});




// Get all users -FOR TEST-

$app->get('/users', function () {
    $userList = DB::query("SELECT id, name FROM users");
    echo json_encode($userList, JSON_PRETTY_PRINT);
});





// Retreive user by their email
$app->get('/users/:email', function($email) use ($app) {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    // only allow to view one's own record
    if ($user['id'] == $authUser['id']) {
        unset($user['password']); // do NOT send password back
        echo json_encode($user, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});





// TODO: Get the game history of the authenticated user
// Add a friend to the my friends, by entering id
// FIXME: verify friend data is valid
// FIXME: verify friend data is already in list

$app->post('/friends', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify user data is valid
    $result = isUserValid($data);
    if ($result !== TRUE) {
        echo json_encode("400 - " . $result);
        $app->response()->status(400);
        return;
    }
    // make sure email is not already in use
    $isIdInUse = DB::queryFirstField("SELECT COUNT(*) FROM users WHERE id=%i", $data['id']);
    if ($isIdInUse) {
        DB::insert('friends', $data);
        $app->response()->status(201);
        echo json_encode($friends, JSON_PRETTY_PRINT);
    }
    echo json_encode("400 - id is not exist");
    $app->response()->status(400);
    return;
});











//TESTED because it uses authentication use next code
// Get random user from user list
// FIXME: should never return the requesting user or user currently in a game
$app->get('/randomUser', function () {
    $authUser = getAuthUser();
    if ($authUser === FALSE) {
        $app->response()->status(401);
        echo json_encode("401 - unauthorized (authentication missing or invalid)");
        return;
    }
    while (true) {
        $userList = DB::queryFirstColumn("SELECT id FROM users");
        $count = count($userList);
        $id = $userList[rand(0, $count-1)];
        if ($id != $authUser['id']) {
            break;
        }
    }
    $randomUser = DB::queryFirstRow("SELECT name, id FROM users WHERE id = %s", $id);
    echo json_encode($randomUser, JSON_PRETTY_PRINT);
});




// TESTED WORKS - USE THIS

$app->get('/randomUser/:id', function ($id) {
    
    while (true) {
        $userList = DB::queryFirstColumn("SELECT id FROM users");
        $count = count($userList);
        $randomId = $userList[rand(0, $count-1)];
        if ($randomId != $id) {
            break;
        }
    }
    $randomUser = DB::queryFirstRow("SELECT name, id FROM users WHERE id = %s", $randomId);
    echo json_encode($randomUser, JSON_PRETTY_PRINT);
});



// TESTED WORKS

$app->get('/quota/:id', function ($id) use ($app){
    
    $quota = DB::queryFirstField("SELECT quota FROM users  WHERE id=%s", $id);
    
    if ($quota > 0) {
       
   DB::update('users', array('quota' => $quota-1 ), "id=%s", $id);
      
    }
    else{
        
     echo json_encode($quota, JSON_PRETTY_PRINT);   
    }
   
    echo json_encode($quota, JSON_PRETTY_PRINT);
});






$app->put('/friends/:id', function($id) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify friend data is valid
    // FIXME: fail with 400 if record does not exist
    DB::update('friends', $data, 'id=%d', $id);
    echo json_encode(true);
});




//$app->delete('/friends/:friendId', function($friendId) use($app){
//    DB::delete('friends', 'friendId=%i', $friendId);
//    echo json_encode(DB::affectedRows() != 0);
//});
//FXME

$app->delete('/friends/:friendId', function($id) {
    DB::delete('friends', 'friendId=%d', $id);
    echo json_encode(DB::affectedRows() != 0);
});





$app->get('/friends/:id', function($id) use ($app) {
    $friend = DB::queryFirstRow("SELECT * FROM friends WHERE id=%i", $id);
    if ($friend) {
        echo json_encode($friend, JSON_PRETTY_PRINT);
    } else {
        $app->response()->status(404);
        echo json_encode("404 - not found");
    }
});




// Tested works
//Create a user

$app->post('/createUser', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify friend data is valid
    DB::insert('users', $data);
    $app->response()->status(201);
});


//NEED Test code it
//Update a user name

$app->put('/updateUser/:id', function($id) use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);


    DB::update('users', $data, 'id=%d', $id);
    $app->response()->status(200);
    echo json_encode(true);
});


// TESTED WORKS: Create a game 
$app->post('/createGame', function() use ($app) {
    $json = $app->request()->getBody();
    $data = json_decode($json, TRUE);
    // FIXME: verify friend data is valid
    DB::insert('games', $data);
    $app->response()->status(201);
    $newId = DB::insertId();
    $game = DB::queryFirstRow("SELECT * FROM games WHERE id=%d", $newId);
    echo json_encode($game, JSON_PRETTY_PRINT);
});





//TIMESTAMP
// Every midnight refresh all the quotas Automaticlly 

$app->get('/times', function () {
   
    $time = time();
    $time = $time-18000;
    $mode = $time % 86400;       
    
    $reminingtime = "Remining time to refill the quotas :".(86400 - $mode); 
    
    $redo =  "redo all quotas 5";
  if ($mode == 0){
      
  DB::update('users', array(
  'quota' => 5 ), "name!=%s", '');
  
      }
else{
     echo json_encode($reminingtime, JSON_PRETTY_PRINT);
    }
});




$app->get('/userStats/:id', function ($id) use($app){
    $gameList = DB::query("SELECT * "
                    . "FROM stats,users "
                    ."WHERE users.id=stats.userId");
    echo json_encode($gameList, JSON_PRETTY_PRINT);
});


$app->run();
