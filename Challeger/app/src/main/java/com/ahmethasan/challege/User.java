package com.ahmethasan.challege;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// entity
public class User {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("quota")
    @Expose
    public int quota;

    @Override
    public String toString() {
        return String.format("%s: %s ", id, name);
    }


    public User(String id, String name, int quota) {
        this.id = id;
        this.name = name;
        this.quota = quota;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public String  getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
