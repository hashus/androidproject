package com.ahmethasan.challege;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class HistoryFragment extends Fragment {
//
//    ArrayList<Games> gamesList = new ArrayList<>();
//    ArrayAdapter<Games> gamesAdapter;

    ListView lvHistory;

    View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_history,container,false);

//
//      //  Games.TaskStatus taskStatus =
//        Games game1 = new Games(1,23,35,87,71,Games.TaskStatus.battery,true);
//        Games game2 = new Games(2,23,35,37,61,Games.TaskStatus.battery,true);
//        Games game3 = new Games(3,17,45,23,11,Games.TaskStatus.warmer,true);
//        Games game4 = new Games(4,17,45,12,23,Games.TaskStatus.warmer,true);
//        Games game5 = new Games(5,37,23,5,13,Games.TaskStatus.colder,true);
//        Games game6 = new Games(6,37,23,4,-5,Games.TaskStatus.colder,true);
//        gamesList.add(game1);
//        gamesList.add(game2);
//        gamesList.add(game3);
//        gamesList.add(game4);
//        gamesList.add(game5);
//        gamesList.add(game6);
//
//
//        lvHistory =  view.findViewById(R.id.lvHistory);
//        gamesAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, gamesList);
//        lvHistory.setAdapter(gamesAdapter);

        return view;
    }
}
