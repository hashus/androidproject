package com.ahmethasan.challege;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;


    //ADDED

    public static final String TAG = "MainActivity";
    public static final String BASE_URL = "https://challengergameapp.000webhostapp.com/api.php/";

    UserService userService;
    GameService gameService;
    StatService statService;
    HistoryService historyService;


    private FirebaseAuth mAuth;
    FirebaseUser firebaseUser;


    public static final int QUOTA = 5;
    public static final String ID = "id";

    SharedPreferences sharedPreferences;

    TextView tvGame;
    TextView tvDailyQuota;

    //


    // For Push Notification

    Button btNotification;
    String oneSignalConnectId;
    public static int Device_Width;
    //


    User user;
    User randomUser;
    Stat stat;
    int quota;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // OneSignal Initialization For Push Notification

        OneSignal.startInit(this)
                .setNotificationReceivedHandler(new ExampleNotificationReceivedHandler())
                .setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        //
        // For Push Notification

        //btNotification = (Button) findViewById(R.id.btNotification);
        //


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);     // new Nav...


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        //ADDED

        tvGame = (TextView) findViewById(R.id.tvGame);
        tvDailyQuota = (TextView) findViewById(R.id.tvDailyQuota);


        startRetrofit();


        mAuth = FirebaseAuth.getInstance();

        Task<AuthResult> authResultTask = mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInAnonymously:success");
                            firebaseUser = mAuth.getCurrentUser();

                            updateUI(firebaseUser);


                            if (!displayData().getId().equals(firebaseUser.getUid())) {

                                user = saveInfo(firebaseUser);

                                createNewUser(user);


                                Log.e("Create", "User is created with id number: " + firebaseUser.getUid() + "\nuser name: " + user.getName());


                            } else {

                                Log.e("Create", "User is already exist with id number: " + firebaseUser.getUid() + "\nuser name: " + displayData().getName());
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                            updateUI(null);


                        }

                        // ...
                    }
                });


        //Set the tags for current user FOR PUSH NOTIFICATION

        DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
        Device_Width = metrics.widthPixels;

        oneSignalConnectId = displayData().id;

        OneSignal.sendTag("USER_ID", oneSignalConnectId);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_challenge:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ChallengeFragment()).commit();
                break;
            case R.id.nav_stats:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new StatsFragment()).commit();
                break;
            case R.id.nav_history:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new HistoryFragment()).commit();
                break;
            case R.id.nav_pending:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PendingFragment()).commit();
                break;
            case R.id.nav_top:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TopFragment()).commit();
                break;
            case R.id.nav_share:

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "More friends more fun! \n Let your friends know about the app ;) \n www.challenger.com/download");
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));


                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;  // select the item true
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //ADDED

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);


    }

    private void updateUI(FirebaseUser firebaseUser) {
        if (firebaseUser == null) {
            Toast.makeText(this, "This is a new install", Toast.LENGTH_SHORT).show();
        }
    }

    private String generateRandomName() {

        // Do something
        String contain = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(5);
        for (int i = 0; i < 5; i++) {

            sb.append(contain.charAt(rnd.nextInt(contain.length())));
        }

        String randomName = "Guest" + sb.toString();

        return randomName;
    }

    public User saveInfo(FirebaseUser firebaseUser) {

        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        String id = firebaseUser.getUid();
        editor.putString("id", id);
        String name = generateRandomName();
        editor.putString("name", name);
        editor.apply();

        User user = new User(id, name, QUOTA);

        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();


        return user;

    }

    public User displayData() {

        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        String id = sharedPreferences.getString("id", "");
        String name = sharedPreferences.getString("name", "");

        User user = new User();

        user.setId(id);
        user.setName(name);
        user.setQuota(QUOTA);

        return user;
    }

    public void startRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                //   .client(okHttpClient)
                .build();

        userService = retrofit.create(UserService.class);
        gameService = retrofit.create(GameService.class);
        statService = retrofit.create(StatService.class);
        historyService = retrofit.create(HistoryService.class);

    }

    //Retrofit
    public void createNewUser(User user) {

        Call<User> callCreateNewUser = userService.createUser(user);
        callCreateNewUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Created new user success", Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fetching users failed (2)", Toast.LENGTH_LONG).show();
            }
        });
    }


    //Retrofit
    public int getQuotaNumber() {

        Call<Integer> callGetDailyQuota = userService.getDailyQuota(displayData().id);


        callGetDailyQuota.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Fetching Quota Number is success", Toast.LENGTH_LONG).show();
                    // DONE: normally you'd put data into ListView here
                    Log.e("Get randomly user", "Get randomly user2");

                    quota = response.body();

                    if (quota > 0) {
                        Toast.makeText(MainActivity.this, "Your daily quota is: " + quota, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Sorry your daily quota is: " + quota, Toast.LENGTH_SHORT).show();
                    }


                } else
                    Toast.makeText(MainActivity.this, "Fetching  Quota Number is failed (1)", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fetching  Quota Number is failed (2)" + displayData().id, Toast.LENGTH_LONG).show();
            }
        });
        return quota;

    }

    //Retrofit
    public void createNewGameOnClick(View view) {

        String user1Id = displayData().id;
        String user2Id = tvGame.getText().toString();


        Game game = new Game(0, user1Id, user2Id, 11, 0, Game.TaskType.colder, Game.TaskState.pending);

        tvGame.setText(game.toString());

        Call<Game> callCreateNewGame = gameService.createGame(game);
        callCreateNewGame.enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Created a new game success", Toast.LENGTH_LONG).show();
                    Log.e("Create Game", "Created a new game success");


                    Globals.currentGame = response.body();

                    //Each game less 1 to the daily quota
                    sendNotificationOnClick();

                    int reminingQuota = getQuotaNumber()-1;
                    String quotaText = "DAILY QUOTA\n"+reminingQuota+"/5";
                    tvDailyQuota.setText(quotaText);
                } else
                    Toast.makeText(MainActivity.this, "Creating game failed (1)", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Creating game failed (2)", Toast.LENGTH_LONG).show();
                Log.e("Create Game", "Created a new game failed(2)", t);
            }
        });
    }

    //Retrofit
    public void getRandomUserOnClick(View view) {


        Call<User> callGetRandomUser = userService.getRandomUser(displayData().id);


        callGetRandomUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Fetching randomly user success", Toast.LENGTH_LONG).show();
                    // DONE: normally you'd put data into ListView here
                    Log.e("Get randomly user", "Get randomly user2");

                    randomUser = response.body();


                    Toast.makeText(MainActivity.this, randomUser.toString(), Toast.LENGTH_SHORT).show();

                    tvGame.setText(randomUser.getId());


                } else
                    Toast.makeText(MainActivity.this, "Fetching randomly users failed (1)", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fetching randomly users failed (2)" + displayData().id, Toast.LENGTH_LONG).show();
            }
        });


    }

    public void sendNotificationOnClick() {


        Toast.makeText(this, "Current Recipients is : " + oneSignalConnectId + " ( Just For Demo )", Toast.LENGTH_SHORT).show();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                int SDK_INT = android.os.Build.VERSION.SDK_INT;
                if (SDK_INT > 8) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                            .permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    String send_id = null;

                    //This is a Simple Logic to Send Notification different Device Programmatically....

                    if (randomUser.id == null) {
                        try {

                            send_id = oneSignalConnectId;
                            Log.e("TAG", "CANNOT sent push notification, because there is no opponent ");

                        } catch (NullPointerException ex) {

                            Log.e("TAG", "CANNOT sent push notification, because there is no opponent ");
                            return;
                        }


                    } else {
                        send_id = randomUser.id;
                        Log.e("TAG", "push notification sent to random user");
                    }

                    try {
                        String jsonResponse;


                        //https://onesignal.com/api/v1/notifications
                        URL url = new URL("https://onesignal.com/api/v1/notifications");  //https://challengergameapp.000webhostapp.com/api.php/

                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        con.setUseCaches(false);
                        con.setDoOutput(true);
                        con.setDoInput(true);

                        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        con.setRequestProperty("Authorization", "Basic YzQ2OWM3OGMtNGY3MS00MjMwLWI5ZDEtMTY0YzAzMjYxY2Ez");
                        con.setRequestMethod("POST");

                        String strJsonBody = "{"
                                + "\"app_id\": \"aae4ebc9-26df-4df5-ac01-e8c5558b10ed\","

                                + "\"filters\": [{\"field\": \"tag\", \"key\": \"USER_ID\", \"relation\": \"=\", \"value\": \"" + send_id + "\"}],"

                                + "\"data\": {\"You have a Challenge request from Guest\": \"Are you ready?\"},"
                                + "\"contents\": {\"en\": \"Heyy!, You have a Challenge request\"}"
                                + "}";

                        //+ "\"contents\": {\"en\": \"You have a Challenge request from\"" + user.getName() + "\"}"


                        System.out.println("strJsonBody:\n" + strJsonBody);

                        byte[] sendBytes = strJsonBody.getBytes("UTF-8");
                        con.setFixedLengthStreamingMode(sendBytes.length);

                        OutputStream outputStream = con.getOutputStream();
                        outputStream.write(sendBytes);

                        int httpResponse = con.getResponseCode();
                        System.out.println("httpResponse: " + httpResponse);

                        if (httpResponse >= HttpURLConnection.HTTP_OK
                                && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                            Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                            scanner.close();
                        } else {
                            Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                            jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                            scanner.close();
                        }
                        System.out.println("jsonResponse:\n" + jsonResponse);

                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        });
    }


    private class ExampleNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            JSONObject data = notification.payload.additionalData;
            String notificationID = notification.payload.notificationID;
            String title = notification.payload.title;
            String body = notification.payload.body;
            String smallIcon = notification.payload.smallIcon;
            String largeIcon = notification.payload.largeIcon;
            String bigPicture = notification.payload.bigPicture;
            String smallIconAccentColor = notification.payload.smallIconAccentColor;
            String sound = notification.payload.sound;
            String ledColor = notification.payload.ledColor;
            int lockScreenVisibility = notification.payload.lockScreenVisibility;
            String groupKey = notification.payload.groupKey;
            String groupMessage = notification.payload.groupMessage;
            String fromProjectNumber = notification.payload.fromProjectNumber;
            String rawPayload = notification.payload.rawPayload;

            String customKey;

            Log.i("OneSignalExample", "NotificationID received: " + notificationID);

            if (data != null) {
                customKey = data.optString("customkey", null);
                if (customKey != null)
                    Log.i("OneSignalExample", "customkey set with value: " + customKey);
            }
        }
    }


    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl

            String customKey;
            String openURL = null;
            Object activityToLaunch = MainActivity.class;

            if (data != null) {
                int batteryStatus = data.optInt("batteryStatus");

                customKey = data.optString("customkey", null);
                openURL = data.optString("openURL", null);

                if (customKey != null)
                    Log.i("OneSignalExample", "customkey set with value: " + customKey);

                if (openURL != null)
                    Log.i("OneSignalExample", "openURL to webview with URL value: " + openURL);
            }

            if (actionType == OSNotificationAction.ActionType.ActionTaken) {
                Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);

                if (result.action.actionID.equals("id1")) {
                    Log.i("OneSignalExample", "button id called: " + result.action.actionID);
                    activityToLaunch = GreenActivity.class;
                } else
                    Log.i("OneSignalExample", "button id called: " + result.action.actionID);
            }
            // The following can be used to open an Activity of your choice.
            // Replace - getApplicationContext() - with any Android Context.
            // Intent intent = new Intent(getApplicationContext(), YourActivity.class);
            Intent intent = new Intent(getApplicationContext(), (Class<?>) activityToLaunch);
            // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("openURL", openURL);
            Log.i("OneSignalExample", "openURL = " + openURL);
            // startActivity(intent);
            startActivity(intent);

            // Add the following to your AndroidManifest.xml to prevent the launching of your main Activity
            //   if you are calling startActivity above.
        /*
           <application ...>
             <meta-data android:name="com.onesignal.NotificationOpened.DEFAULT" android:value="DISABLE" />
           </application>
        */
        }
    }

    //Retrofit
    public Stat getUserStats() {


        Call<Stat> callGetUserStats = statService.getUserStats(displayData().id);


        callGetUserStats.enqueue(new Callback<Stat>() {
            @Override
            public void onResponse(Call<Stat> call, Response<Stat> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Fetching randomly user success", Toast.LENGTH_LONG).show();
                    // DONE: normally you'd put data into ListView here
                    Log.e("Get randomly user", "Get randomly user2");

                    stat = response.body();


                    Toast.makeText(MainActivity.this, randomUser.toString(), Toast.LENGTH_SHORT).show();

                    tvGame.setText(randomUser.getId());


                } else
                    Toast.makeText(MainActivity.this, "Fetching randomly users failed (1)", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Stat> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fetching randomly users failed (2)" + displayData().id, Toast.LENGTH_LONG).show();
            }
        });

        return stat;
    }

    //Retrofit
    public void getGameHistory() {

        Call<List<Game>> callGetGameHistory = historyService.getgameHistory(displayData().id);


        callGetGameHistory.enqueue(new Callback<List<Game>>() {
            @Override
            public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Fetching randomly user success", Toast.LENGTH_LONG).show();
                    // DONE: normally you'd put data into ListView here
                    Log.e("Get randomly user", "Get randomly user2");

                    List<Game> gameHistory = response.body();

                    for (Game game : gameHistory) {
                        Log.d(TAG, "Game History: " + game);

                    }


                    Toast.makeText(MainActivity.this, randomUser.toString(), Toast.LENGTH_SHORT).show();

                    tvGame.setText(randomUser.getId());


                } else
                    Toast.makeText(MainActivity.this, "Fetching randomly users failed (1)", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Game>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fetching randomly users failed (2)" + displayData().id, Toast.LENGTH_LONG).show();
            }
        });
    }

}


