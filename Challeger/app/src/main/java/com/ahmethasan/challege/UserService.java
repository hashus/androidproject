package com.ahmethasan.challege;


import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

    public interface UserService {


        @GET("users")
        Call<List<User>> getAllUsers();

        @GET("randomUser/{id}")
        Call<User> getRandomUser(@Path("id") String id);

        @GET("topTenUser")
        Call<List<User>> getTopTenUser();

        @GET("quota/{id}")
        Call<Integer> getDailyQuota(@Path("id") String id);

        @POST("createUser")
        Call<User> createUser(@Body User user);

        @PUT("updateUser/{id}")
        Call<User> updateUser(@Path("id") String id, @Body User user);

        @PUT("deleteFriend/{id}")
        Call<User> deleteFriend(@Path("id") String id);



        //Get all users randomly
//        @GET("randomUser")
//        Call<User> getRandomUser();



    }