package com.ahmethasan.challege;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class History {


    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user1Id")
    @Expose
    public String user1Id;

    @SerializedName("user2Id")
    @Expose
    public String user2Id;

    @SerializedName("user1Value")
    @Expose
    public int user1Value;

    @SerializedName("user2Value")
    @Expose
    public int user2Value;

    @SerializedName("user1Name")
    @Expose
    public String user1Name;

    @SerializedName("user2Name")
    @Expose
    public String user2Name;


    TaskType type;

    enum TaskType {
        @SerializedName("battery") battery,
        @SerializedName("warmer") warmer,
        @SerializedName("colder") colder,
        @SerializedName("speed") speed

    }


    TaskState state;

    enum TaskState {
        @SerializedName("pending") pending,
        @SerializedName("done") done
    }

    @Override
    public String toString() {
        return String.format("%d %s %s %d %d %s %s", id, user1Name, user2Name, user1Value, user2Value, type , state);
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser1Id() {
        return user1Id;
    }

    public void setUser1Id(String user1Id) {
        this.user1Id = user1Id;
    }

    public String getUser2Id() {
        return user2Id;
    }

    public void setUser2Id(String user2Id) {
        this.user2Id = user2Id;
    }

    public int getUser1Value() {
        return user1Value;
    }

    public void setUser1Value(int user1Value) {
        this.user1Value = user1Value;
    }

    public int getUser2Value() {
        return user2Value;
    }

    public void setUser2Value(int user2Value) {
        this.user2Value = user2Value;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
    }
}

