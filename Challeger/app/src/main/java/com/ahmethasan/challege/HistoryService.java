package com.ahmethasan.challege;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HistoryService {

    @GET("gameHistory/{id}")
    Call<List<Game>> getgameHistory(@Path("id") String id);

    @GET("incomingChallenges/{user1Id}")
    Call<List<Game>> getincomingChallenges(@Path("id") String id);

    @GET("outgoingChallenges/{user1Id}")
    Call<List<Game>> getoutgoingChallenges(@Path("id") String id);
}
