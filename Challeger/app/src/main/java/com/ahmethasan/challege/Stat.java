package com.ahmethasan.challege;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stat {


    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("points")
    @Expose
    public int points;

    @SerializedName("wins")
    @Expose
    public int wins;

    @SerializedName("losses")
    @Expose
    public int losses;

    @SerializedName("draws")
    @Expose
    public int draws;


    public Stat(String userId, int points, int wins, int losses, int draws) {
        this.userId = userId;
        this.points = points;
        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    @Override
    public String toString() {
        return String.format("%s: %d %d %d %d", userId, points, wins, losses, draws );
    }
}
