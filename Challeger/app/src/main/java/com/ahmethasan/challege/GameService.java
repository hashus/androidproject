package com.ahmethasan.challege;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GameService {


    @GET("games/{user1Id}")
    Call<List<Game>> getFinishedGames();

    @POST("createGame")
    Call<Game> createGame(@Body Game game);

    @GET("games/{user1Id}")
    Call<List<Game>> getTodaysGames();


}
