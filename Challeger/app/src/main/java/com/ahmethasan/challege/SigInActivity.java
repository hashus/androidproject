package com.ahmethasan.challege;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SigInActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final String BASE_URL = "https://challengergameapp.000webhostapp.com/api.php/";

    UserService userService;
    GameService gameService;


    private FirebaseAuth mAuth;
    FirebaseUser firebaseUser;


    public static final int QUOTA = 5;
    public static final String ID = "id";

    SharedPreferences sharedPreferences;


    Button btPlay;
    TextView tvGame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sig_in);


        btPlay = (Button) findViewById(R.id.btPlay);
        tvGame = (TextView) findViewById(R.id.tvGame);


        startRetrofit();


        mAuth = FirebaseAuth.getInstance();

        Task<AuthResult> authResultTask = mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInAnonymously:success");
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();

                            updateUI(firebaseUser);


                            if (!displayData().getId().equals(firebaseUser.getUid())) {

                                User user = saveInfo(firebaseUser);

                                createNewUser(user);


                                Log.e("Create", "User is created with id number: " + firebaseUser.getUid() + "\nuser name: " + user.getName());


                            } else {

                                Log.e("Create", "User is already exist with id number: " + firebaseUser.getUid() + "\nuser name: " + displayData().getName());
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(SigInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                            updateUI(null);


                        }

                        // ...
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);


    }


    private void updateUI(FirebaseUser firebaseUser) {
        if (firebaseUser == null) {
            Toast.makeText(this, "This is a new install", Toast.LENGTH_SHORT).show();
        }
    }


    private String generateRandomNameOnClick() {

        // Do something
        String contain = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(5);
        for (int i = 0; i < 5; i++) {

            sb.append(contain.charAt(rnd.nextInt(contain.length())));
        }

        String randomName = "Guest" + sb.toString();

        return randomName;
    }


    public User saveInfo(FirebaseUser firebaseUser) {

        sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        String id = firebaseUser.getUid();
        editor.putString("id", id);
        String name = generateRandomNameOnClick();
        editor.putString("name", name);
        editor.apply();

        User user = new User(id, name, QUOTA);

        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();


        return user;

    }


    public User displayData() {

        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);

        String id = sharedPreferences.getString("id", "");
        String name = sharedPreferences.getString("name", "");

        User user = new User();

        user.setId(id);
        user.setName(name);
        user.setQuota(QUOTA);


        return user;


    }

    public void startRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                //   .client(okHttpClient)
                .build();

        userService = retrofit.create(UserService.class);
        gameService = retrofit.create(GameService.class);
    }


    public void createNewUser(User user) {

        Call<User> callCreateNewUser = userService.createUser(user);
        callCreateNewUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(SigInActivity.this, "Created new user success", Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(SigInActivity.this, "Fetching users failed (2)", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void createNewGame(View view) {

        String user1Id = displayData().id;
        String user2Id = tvGame.getText().toString();


        Game game = new Game(0, user1Id, user2Id, 11, 9, Game.TaskType.colder, Game.TaskState.pending);

        tvGame.setText(game.toString());

        Call<Game> callCreateNewGame = gameService.createGame(game);
        callCreateNewGame.enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(SigInActivity.this, "Created a new game success", Toast.LENGTH_LONG).show();
                    Log.e("Create Game", "Created a new game success");
                    Globals.currentGame = response.body();
                } else
                    Toast.makeText(SigInActivity.this, "Creating game failed (1)", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {
                Toast.makeText(SigInActivity.this, "Creating game failed (2)", Toast.LENGTH_LONG).show();
                Log.e("Create Game", "Created a new game failed(2)", t);
            }
        });
    }


    public void getRandomUserOnClick(View view) {


        Call<User> callGetRandomUser = userService.getRandomUser(displayData().id);


        callGetRandomUser.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(SigInActivity.this, "Fetching randomly user success", Toast.LENGTH_LONG).show();
                    // DONE: normally you'd put data into ListView here
                    Log.e("Get randomly user", "Get randomly user2");

                    User randomUser = response.body();


                    Toast.makeText(SigInActivity.this, randomUser.toString(), Toast.LENGTH_SHORT).show();

                    tvGame.setText(randomUser.getId());


                } else
                    Toast.makeText(SigInActivity.this, "Fetching randomly users failed (1)", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(SigInActivity.this, "Fetching randomly users failed (2)", Toast.LENGTH_LONG).show();
            }
        });


    }


}

