package com.ahmethasan.challege;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface StatService {

    @GET("userStats/{userId}")
    Call<Stat> getUserStats(@Path("userId") String userId);
}
